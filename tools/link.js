import { existsSync, readFileSync } from "fs";
import { symlink } from "fs/promises";
import { join, resolve } from "path";

const config = JSON.parse(readFileSync("foundry-config.json", "utf-8"));

if (!config.dataPath) {
  throw Error("No user Data path defined in foundry-config.json");
}

if (!existsSync(config.dataPath)) {
  throw Error("User Data path doesn't exist");
}

const linkDir = join(config.dataPath, "systems/fist-ultra-edition");

await symlink(resolve("./dist"), linkDir, "dir");
