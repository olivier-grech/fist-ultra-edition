import copy from "@guanghechen/rollup-plugin-copy";
import { defineConfig } from "rollup";
import postcss from "rollup-plugin-postcss";

const name = "fist-ultra-edition";
const distDirectory = "dist";
const srcDirectory = "src";

const staticFiles = ["assets", "lang", "templates", "system.json"];

export default defineConfig({
  input: { [`${name}`]: `${srcDirectory}/${name}.js` },
  output: {
    dir: distDirectory,
    format: "es",
    assetFileNames: "[name].[ext]",
  },
  plugins: [
    postcss({
      extract: true,
      minimize: true,
      use: ["sass"],
    }),
    copy({
      targets: [
        {
          src: staticFiles.map((f) => `${srcDirectory}/${f}`),
          dest: distDirectory,
        },
      ],
    }),
  ],
});
