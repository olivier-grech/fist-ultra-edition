# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.13.0] - 2025-01-04
### Added
- Add button to display item in chat.

### Changed
- Change item hints style.

## [1.12.0] - 2024-11-12
### Added
- Add fields for limited in weapon and armor sheets.

## [1.11.0] - 2024-11-07
### Added
- Add fields for limited uses and accessory in item sheets.

### Changed
- Use distinct Item types for weapons and armors.
- Reduce font size for item hints.

## [1.10.0] - 2024-10-28
### Added
- Message for items with no description.
- Ellipsis for items with long name.

### Changed
- Change style of damage label.

## [1.9.0] - 2024-10-27
### Changed
- Rename equipment to gear.
- Add back sheet for outdated "trait" Item type.

## [1.8.0] - 2024-09-25
### Changed
- Rename traits to skills.

### Fixed
- Fix sheet breaking when deleting an item when its description is displayed.
- Fix item description being displayed as undefined for newly created items.

## [1.7.0] - 2024-09-15
### Added
- Display damage rolls terms.
- Update verified Foundry VTT version to 12.331.

## [1.6.0] - 2024-09-13
### Changed
- Adjust color contrast of the sheet.
- Use custom style for chat messages.
- Remove roll modifier from chat messages when the modifier is 0.

## [1.5.0] - 2024-09-07
### Added
- Setup image.

### Changed
- Change the internal organization of the project. This should slightly reduce the size of the game system.

## [1.4.0] - 2024-08-11
### Added
- Compatibility with Foundry VTT v12.
- Update minimum Foundry VTT version to 12.
- Update verified Foundry VTT version to 12.330.

### Changed
- Adjust style of roll messages.
- Migrate data schema definition from template.json to System Data Models.
- Use a separate SCSS sheet for generic styles.

### Fixed
- Fix HP not being able to be displayed as a resource.

## [1.3.0] - 2024-04-28
### Added
- Damage rolls.
- Attribute name in attribute rolls.

### Changed
- Slightly reduce attribute inputs width.

## [1.2.0] - 2024-04-22
### Added
- Attribute rolls.

### Changed
- Upgrade verified version to 11.315.

### Fixed
- Fix padding in Codename, Pronouns and Role text fields.

## [1.1.0] - 2023-09-10
### Added
- Description field for traits and items.

### Changed
- Remove use of Noto Sans font.
- Disable spellcheck for Codename field.

### Fixed
- Fix non-number input allowed for Armor, War Dice, Max HP and HP fields.

## [1.0.0] - 2023-09-08
### Added
- Initial version.
