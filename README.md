# Foundry Virtual Tabletop - FIST: Ultra Edition Game System

*FIST: Ultra Edition is a tabletop roleplaying game about paranormal mercenaries doing the tough jobs no one else can. In the game, you belong to a legendary rogue mercenary unit called FIST. You are a soldier of fortune who doesn’t fit into modern society. You are a disposable gun for hire, caught up in the death and destruction of pointless proxy wars and oppressive establishments. You may also be someone who can turn into a ghost or control bees with your mind.*

This unofficial game system for [Foundry Virtual Tabletop](https://foundryvtt.com) provides a character sheet and support for the core rules of the game.

## Screenshots

![Screenshot](screenshots/screenshot.jpg)

## License

FIST: Ultra Edition is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License available at https://creativecommons.org/licenses/by-sa/4.0/legalcode.

The software component of this system is distributed under the MIT license.

## Support Me

[![Buy Me a Coffee](./bmc-button.svg)](https://www.buymeacoffee.com/oliviergrech)
