export class FistUltraEditionItemSheet extends ItemSheet {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["fist-ultra-edition", "sheet", "item"],
    });
  }

  get template() {
    return `systems/fist-ultra-edition/templates/items/${this.item.type}-sheet.html`;
  }

  /** @override */
  getData() {
    const data = super.getData();
    const itemData = data.item;
    data.system = itemData.system;
    return data;
  }
}
