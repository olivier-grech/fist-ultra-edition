export async function displayItem(item) {
  let result = await renderTemplate(
    "systems/fist-ultra-edition/templates/display/display-item.html",
    {
      item_name: item["name"],
      item_description: item["system"]["description"],
    }
  );
  ChatMessage.create({
    speaker: ChatMessage.getSpeaker(),
    content: result,
  });
  ui.chat.scrollBottom();
}
