const { BooleanField, NumberField, SchemaField, StringField } =
  foundry.data.fields;

export class GearData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new StringField({ required: true }),
      isLimitedUses: new BooleanField({}),
      uses: new SchemaField({
        value: new NumberField({
          integer: true,
          min: 0,
          initial: 0,
        }),
        min: new NumberField({
          integer: true,
          min: 0,
          initial: 0,
        }),
        max: new NumberField({
          integer: true,
          min: 0,
          initial: 1,
        }),
      }),
      damage: new StringField({}),
      armor: new NumberField({
        integer: true,
        min: 0,
        initial: 0,
      }),
    };
  }
}
