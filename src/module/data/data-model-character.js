const { NumberField, SchemaField, StringField } = foundry.data.fields;

export class CharacterData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      pronouns: new StringField({}),
      role: new StringField({}),
      armor: new NumberField({
        integer: true,
        min: 0,
        initial: 0,
      }),
      warDice: new NumberField({
        integer: true,
        min: 0,
        initial: 0,
      }),
      hp: new SchemaField({
        value: new NumberField({
          integer: true,
          min: 0,
          initial: 6,
        }),
        min: new NumberField({
          integer: true,
          min: 0,
          initial: 0,
        }),
        max: new NumberField({
          integer: true,
          min: 0,
          initial: 6,
        }),
      }),
      attributes: new SchemaField({
        forceful: new SchemaField({
          value: new NumberField({
            integer: true,
            initial: 0,
          }),
          label: new StringField({
            initial: "FISTULTRAEDITION.Forceful",
          }),
        }),
        tactical: new SchemaField({
          value: new NumberField({
            integer: true,
            initial: 0,
          }),
          label: new StringField({
            initial: "FISTULTRAEDITION.Tactical",
          }),
        }),
        creative: new SchemaField({
          value: new NumberField({
            integer: true,
            initial: 0,
          }),
          label: new StringField({
            initial: "FISTULTRAEDITION.Creative",
          }),
        }),
        reflexive: new SchemaField({
          value: new NumberField({
            integer: true,
            initial: 0,
          }),
          label: new StringField({
            initial: "FISTULTRAEDITION.Reflexive",
          }),
        }),
      }),
    };
  }

  static migrateData(source) {
    // Migrate from 1.3.0 to 1.4.0 (hp.current renamed to hp.value)
    if (source.hp.current != null) {
      source.hp.value = source.hp.current;
      source.hp.current = null;
    }
  }
}
