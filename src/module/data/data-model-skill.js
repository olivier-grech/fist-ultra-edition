const { StringField } = foundry.data.fields;

export class SkillData extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    return {
      description: new StringField({ required: true }),
    };
  }
}
