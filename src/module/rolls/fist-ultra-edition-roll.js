export async function attributeRoll(attribute) {
  let roll = new Roll(`2d6`, {});
  await roll.evaluate();

  let modifier = parseInt(attribute.value);
  let roll_total = roll.total + modifier;
  let first_dice = roll.dice[0].results[0].result;
  let second_dice = roll.dice[0].results[1].result;
  let text = "";

  let attribute_label = attribute.label;

  if (first_dice == 6 && second_dice == 6) {
    text =
      "Your roll is an <strong>ultra success</strong>! You do exactly what you wanted to do, with some spectacular added bonus";
  } else if (roll_total <= 6) {
    text =
      "Your roll is a <strong>failure</strong>. You don't do what you wanted to do, and things go wrong somehow.";
  } else if (roll_total >= 7 && roll_total <= 9) {
    text =
      "Your roll is a <strong>partial success</strong>. You do what you wanted to, but with a cost, compromise, or complication.";
  } else if (roll_total >= 10) {
    text =
      "Your roll is a <strong>success</strong>. You do exactly what you wanted to do, without any additional headaches.";
  }

  let result = await renderTemplate(
    "systems/fist-ultra-edition/templates/rolls/attribute-roll-result.html",
    {
      attribute_label: attribute_label,
      roll_dice: roll.dice[0].results,
      roll_modifier: attribute.value,
      roll_total: roll_total,
      text: text,
    }
  );
  ChatMessage.create({
    speaker: ChatMessage.getSpeaker(),
    content: result,
    rolls: [roll],
  });
  ui.chat.scrollBottom();
}

export async function damageRoll(item) {
  let item_name = item.name;
  let item_damage = item.system["damage"];

  let roll = new Roll(item_damage, {});
  if (!validateDamageRoll(roll)) {
    // For now, do nothing if the damage roll is invalid
    return;
  }

  await roll.evaluate();

  let roll_dice = null;
  if (roll.terms.length != 1 || getTermType(roll.terms[0]) != "NumericTerm") {
    roll_dice = roll.terms[0].results;
  }

  let roll_modifier;
  if (roll.terms.length == 1) {
    roll_modifier = null;
  } else {
    roll_modifier = roll.terms[1].operator + roll.terms[2].number;
  }

  let roll_total = Math.max(roll.total, 0);

  let result = await renderTemplate(
    "systems/fist-ultra-edition/templates/rolls/damage-roll-result.html",
    {
      item_name: item_name,
      item_damage: item_damage,
      roll_dice: roll_dice,
      roll_modifier: roll_modifier,
      roll_total: roll_total,
    }
  );
  ChatMessage.create({
    speaker: ChatMessage.getSpeaker(),
    content: result,
    rolls: [roll],
  });
}

function getTermType(term) {
  return Object.getPrototypeOf(term).constructor.name;
}

// A damage roll should be one of the three following forms:
// - A numeric term (e.g. 3)
// - A die term (e.g. 2d6)
// - A die term, an operator term and a numeric term (e.g. 2d6+1)
function validateDamageRoll(roll) {
  if (roll.terms.length == 1) {
    let termType = getTermType(roll.terms[0]);
    return termType == "Die" || termType == "NumericTerm";
  }

  if (roll.terms.length == 3) {
    return (
      getTermType(roll.terms[0]) == "Die" &&
      getTermType(roll.terms[1]) == "OperatorTerm" &&
      getTermType(roll.terms[2]) == "NumericTerm"
    );
  }

  return false;
}
