import { attributeRoll, damageRoll } from "../rolls/fist-ultra-edition-roll.js";
import { displayItem } from "../display/display-item.js";

export class FistUltraEditionCharacterSheet extends ActorSheet {
  /**
   * IDs for items on the sheet that have been expanded.
   * @type {Set<string>}
   * @protected
   */
  _expanded = new Set();

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["fist-ultra-edition", "sheet", "character"],
      template:
        "systems/fist-ultra-edition/templates/actors/character-sheet.html",
      width: 600,
      height: 800,
    });
  }

  /** @override */
  getData() {
    const data = super.getData();
    const actorData = this.actor.toObject(false);
    data.system = actorData.system;

    // Prepare items
    if (actorData.type == "character") {
      this._prepareCharacterItems(data);
    }

    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    if (!this.options.editable) {
      return;
    }

    // Add uses buttons
    html.find(".uses-counter").click((event) => {
      const boxItem = $(event.currentTarget).parents(".fist-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      if (item.system.uses.value < item.system.uses.max) {
        let update_object = {
          system: { uses: { value: item.system.uses.value + 1 } },
        };
        item.update(update_object);
      }
    });

    // Remove uses buttons
    html.find(".uses-counter").on("contextmenu", (event) => {
      const boxItem = $(event.currentTarget).parents(".fist-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      if (item.system.uses.value > item.system.uses.min) {
        let update_object = {
          system: { uses: { value: item.system.uses.value - 1 } },
        };
        item.update(update_object);
      }
    });

    // Add roll buttons
    html.find(".roll-attribute").click((event) => {
      let attributeKey = event.currentTarget.dataset.attribute;
      let attribute = this.actor.system.attributes[attributeKey];
      attributeRoll(attribute);
    });

    // Add item buttons
    html.find(".fist-item-create-skills").click(this._onSkilLCreate.bind(this));
    html
      .find(".fist-item-create-inventory")
      .click(this._openItemCreationDialog.bind(this));

    // Add attack buttons
    html.find(".fist-item-attack").click((event) => {
      const boxItem = $(event.currentTarget).parents(".fist-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      damageRoll(item);
    });

    // Edit item buttons
    html.find(".fist-item-edit").click((event) => {
      const boxItem = $(event.currentTarget).parents(".fist-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      item.sheet.render(true);
    });

    // Delete item buttons
    html.find(".fist-item-delete").click((event) => {
      const boxItem = $(event.currentTarget).parents(".fist-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      this._expanded.delete(item.id);
      item.delete();
    });

    // Display item buttons
    html.find(".fist-item-display").click((event) => {
      const boxItem = $(event.currentTarget).parents(".fist-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      displayItem(item);
    });

    // Hide or show item description
    html.find(".table-item-name").click((event) => this._onItemSummary(event));

    for (let itemId of this._expanded) {
      const boxItem = html.find(`[data-item-id="${itemId}"]`);
      const item = this.actor.items.get(itemId);
      this._expandItemDescription(boxItem, item);
      boxItem.toggleClass("expanded");
    }
  }

  _onSkilLCreate() {
    this._onIemCreate("skill");
  }

  _onIemCreate(type) {
    Item.implementation.create(
      { name: `New ${type.capitalize()}`, type: type },
      { parent: this.actor }
    );
  }

  _onItemSummary(event) {
    event.preventDefault();
    const boxItem = $(event.currentTarget).parents(".table-item");
    const item = this.actor.items.get(boxItem.data("itemId"));
    if (boxItem.hasClass("expanded")) {
      let summary = boxItem.children(".table-item-description");
      summary.slideUp(200, () => summary.remove());
      this._expanded.delete(item.id);
    } else {
      this._expandItemDescription(boxItem, item, true);
      this._expanded.add(item.id);
    }
    boxItem.toggleClass("expanded");
  }

  _expandItemDescription(boxItem, item, slide = false) {
    let itemDescription = this._getItemDescription(item);
    if (slide) {
      boxItem.append(itemDescription.hide());
      itemDescription.slideDown(200);
    } else {
      boxItem.append(itemDescription);
    }
  }

  _getItemDescription(item) {
    return $(
      `<div class="table-item-description mb-2">${
        item.system.description
          ? item.system.description
          : "<i>No description.</i>"
      }</div>`
    );
  }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    // Initialize containers.
    const gear = [];
    const skills = [];

    // Iterate through items, allocating to containers
    for (let i of sheetData.items) {
      i.img = i.img || DEFAULT_TOKEN;

      let hint = "";

      if (i.system.isLimitedUses) {
        hint +=
          '&nbsp;<a class="tag uses-counter">' +
          i.system.uses.value +
          "/" +
          i.system.uses.max +
          " uses</a>";
      }

      if (i.type === "weapon") {
        hint +=
          '&nbsp;<a class="tag fist-item-attack">' +
          i.system.damage +
          " damage</a>";
      }

      if (i.type === "armor") {
        hint += '&nbsp;<span class="tag">' + i.system.armor + " armor";
        if (i.system.isAccessory) {
          hint += ", accessory";
        }
        hint += "</span>";
      }

      i.system.hint = hint;

      // "equipment" is the old name of "gear"
      // This is for backward compatibility and might be removed in a future
      // update
      if (
        i.type === "gear" ||
        i.type === "armor" ||
        i.type === "weapon" ||
        i.type === "equipment"
      ) {
        gear.push(i);
      }

      // "trait" is the old name of "skill"
      // This is for backward compatibility and might be removed in a future
      // update
      if (i.type === "skill" || i.type === "trait") {
        skills.push(i);
      }
    }

    // Assign and return
    actorData.gear = gear;
    actorData.skills = skills;
  }

  async _openItemCreationDialog() {
    const itemType = await foundry.applications.api.DialogV2.wait({
      window: { title: "New Item" },
      modal: true,
      buttons: [
        {
          label: "Gear",
          action: "gear",
        },
        {
          label: "Weapon",
          action: "weapon",
        },
        {
          label: "Armor",
          action: "armor",
        },
      ],
    });
    this._onIemCreate(itemType);
  }
}
