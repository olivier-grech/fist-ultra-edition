import { FistUltraEditionItemSheet } from "./module/items/fist-ultra-edition-item-sheet.js";
import { FistUltraEditionCharacterSheet } from "./module/actors/fist-ultra-edition-character-sheet.js";
import { ArmorData } from "./module/data/data-model-armor.js";
import { CharacterData } from "./module/data/data-model-character.js";
import { GearData } from "./module/data/data-model-gear.js";
import { SkillData } from "./module/data/data-model-skill.js";
import { WeaponData } from "./module/data/data-model-weapon.js";
import "./fist-ultra-edition.scss";

Hooks.on("init", async function () {
  console.log(`Initializing FIST: Ultra Edition system`);

  CONFIG.Actor.dataModels.character = CharacterData;
  CONFIG.Item.dataModels.armor = ArmorData;
  CONFIG.Item.dataModels.gear = GearData;
  CONFIG.Item.dataModels.skill = SkillData;
  CONFIG.Item.dataModels.weapon = WeaponData;

  // Register actors sheets
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("fist-ultra-edition", FistUltraEditionCharacterSheet, {
    types: ["character"],
    makeDefault: true,
  });

  // Register items sheets
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("fist-ultra-edition", FistUltraEditionItemSheet, {
    types: ["armor", "equipment", "gear", "skill", "trait", "weapon"],
    makeDefault: true,
  });
});

Handlebars.registerHelper("signedNumber", function (number) {
  if (number > 0) {
    return "+" + number;
  }
  return "" + number;
});
